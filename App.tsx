import React from 'react';
import { View } from 'react-native';

import { gstyles } from './styles/Global';
import { Drawer } from './routes/Drawer';

/**
 * Main entry point or react native app
 */
export default function App() {
    return (
        <View style={ gstyles.container }>
            <Drawer/>
        </View>
    );
}
