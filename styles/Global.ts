import { StyleSheet } from 'react-native';

import { scheme } from './Scheme';

/**
 * Global stylesheet of SleepWell.
 */
export const gstyles = StyleSheet.create({
    /** Default container style */
    container: {
        backgroundColor: scheme.theme.d5.bgcolor,
        flex: 1,
        justifyContent: "flex-start"
    },
    /** Default style of text */
    text: {
        color: scheme.theme.d5.color,
        fontSize: 18
    },
    /** Cards' style */
    cards: {
        justifyContent: "center",
        alignItems: "center",
        marginVertical: 5,
        marginHorizontal: 10
    },
    /** Cards' text style */
    cardsText: {
        textAlign: "center"
    },
    /** Style for week day */
    weekdayTitle: {
        fontSize: 30
    },
    /** Style for last sleep time. */
    lastSleepTime: {
        fontSize: 30,
        fontWeight: "bold"
    },
    /** Style for sleep detail. */
    sleepDetailPanel: {
        flexDirection: "row",
        alignItems: "stretch",
        justifyContent: "space-around",
        width: "100%",
        paddingHorizontal: 5
    },
    /** Style for sleep detail child. */
    sleepDetailPanelChild: {
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: 10
    },
    /** Icon style for sleep detail child. */
    sleepDetailPanelChildIcon: {
        color: "white"
    }
});