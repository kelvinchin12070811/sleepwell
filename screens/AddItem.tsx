import { FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons';
import { useRoute, useNavigation } from '@react-navigation/native';
import React from 'react';
import { View, StyleSheet, ScrollView, Keyboard } from 'react-native';
import { Formik } from 'formik';
import * as yup from 'yup';

import { Card } from '../components/cards/Card';
import { gstyles } from '../styles/Global';
import { scheme } from '../styles/Scheme';
import { ThemedText } from '../components/ThemedText';
import SleepDuration from '../legacy/SleepDuration';
import { TextInput, TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';

/** Props for AddItem */
interface AddItemProp
{
    setItems: (values: SleepDuration) => void;
}

const schema = yup.object({
    date: yup.string()
        .required()
        .test("is-date", "pls enter valid date as yyyy-mm-dd", (value) => {
            return !isNaN((new Date(value)).getTime());
        }),
    sleepTime: yup.string()
        .required()
        .test("is-valid-sleep-time", "pls enter valid time as hh:mm", (value) => {
            return !isNaN(Date.parse(`1970-01-01T${ value }:00`));
        }),
    awakeTime: yup.string()
        .required()
        .test("is-valid-sleep-time", "pls enter valid time as hh:mm", (value) => {
            return !isNaN(Date.parse(`1970-01-01T${ value }:00`));
        }),
    rating: yup.string()
        .required()
        .test("is-num-1-5", "pls enter number between 1 - 5", (value) => {
            const num = parseInt(value);
            return num > 0 && num < 6;
        })
});

/**
 * @brief The component that allow user add sleep data.
 */
export const AddItem: React.FC<AddItemProp> = () => {
    const route = useRoute<any>();
    const navigation = useNavigation();
    
    return(
        <View style={{ ...gstyles.container, paddingVertical: (16-5), paddingHorizontal: 16 }}>
        <TouchableWithoutFeedback onPress={ Keyboard.dismiss }>
        <Card style={{ ...gstyles.cards, alignItems: "stretch" }}>
            <Formik
                initialValues={{
                    date: "",
                    sleepTime: "",
                    awakeTime: "",
                    rating: ""
                }}
                onSubmit={ (values, action) => {
                    const awake = new Date(`1970-01-01T${ values.awakeTime }:00`);
                    const slept = new Date(`1970-01-01T${ values.sleepTime }:00`);
                    let duration = new SleepDuration({ hour: slept.getHours(), minute: slept.getMinutes() },
                        { hour: awake.getHours(), minute: awake.getMinutes() },
                        values.date, parseInt(values.rating));
                    route.params.setItem(duration);
                    navigation.goBack();
                    action.resetForm();
                } }
                validationSchema={ schema }
            >
            {
                props => (
                    <View>
                        <TextInput
                            placeholder="Date"
                            onChangeText={ props.handleChange("date") }
                            value={ props.values.date }
                            onBlur={ props.handleBlur("date") }
                            style={ styles.formInput }
                        />
                        <ThemedText>
                            { props.touched.date && props.errors.date }
                        </ThemedText>
                        <TextInput
                            placeholder="Sleep Time"
                            onChangeText={ props.handleChange("sleepTime") }
                            value={ props.values.sleepTime }
                            onBlur={ props.handleBlur("sleepTime") }
                            style={ styles.formInput }
                        />
                        <ThemedText>
                            { props.touched.sleepTime && props.errors.sleepTime }
                        </ThemedText>
                        <TextInput
                            placeholder="Awake Time"
                            onChangeText={ props.handleChange("awakeTime") }
                            value={ props.values.awakeTime }
                            onBlur={ props.handleBlur("awakeTime") }
                            style={ styles.formInput }
                        />
                        <ThemedText>
                            { props.touched.awakeTime && props.errors.awakeTime }
                        </ThemedText>
                        <TextInput
                            placeholder="Rating"
                            onChangeText={ props.handleChange("rating") }
                            value={ props.values.rating }
                            onBlur={ props.handleBlur("rating") }
                            style={ styles.formInput }
                            keyboardType="number-pad"
                        />
                        <ThemedText>
                            { props.touched.rating && props.errors.rating }
                        </ThemedText>
                        <TouchableOpacity style={ styles.button } onPress={ props.submitForm }>
                            <ThemedText style={ styles.buttonText }>Submit</ThemedText>
                        </TouchableOpacity>
                    </View>
                )
            }
            </Formik>
        </Card>
        </TouchableWithoutFeedback>
        </View>
    );
};

const styles = StyleSheet.create({
    formInput: {
        fontSize: 18,
        color: "white",
        fontWeight: "bold",
        marginVertical: 5
    },
    button: {
        backgroundColor: scheme.theme.l2.bgcolor,
        paddingVertical: 5,
        borderRadius: 30,
        marginVertical: 5
    },
    buttonText: {
        textAlign: "center"
    }
});