import React from 'react';
import { StyleSheet, View } from 'react-native';

import { gstyles } from '../styles/Global';
import { ThemedText } from '../components/ThemedText';

/**
 * @brief About page of SleepWell
 */
export const About: React.FC = () => {
    return (
        <View
            style={{
                ...styles.container,
                ...gstyles.container,
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
            }}
        >
            <ThemedText style={ styles.titleText }>About SleepWell</ThemedText>
            <ThemedText>SleepWell is a minimalist sleep tracking app</ThemedText>
        </View>
    );
};

/**
 * Internal style of About.
 */
const styles = StyleSheet.create({
    /** Container style */
    container: {
        justifyContent: "center",
        alignItems: "center"
    },
    /** Title text style */
    titleText: {
        fontSize: 25,
        fontWeight: "bold",
        marginBottom: 25
    }
});