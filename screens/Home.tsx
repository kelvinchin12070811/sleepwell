import React, { useState } from 'react';
import { View, StyleSheet, Alert } from 'react-native';

import { OverviewCard } from '../components/cards/OverviewCard';
import { WeeklyDeatils } from '../components/cards/WeeklyDetails';
import { AverageDataCard } from '../components/cards/AverageDataCard';
import { gstyles } from '../styles/Global';
import SleepDuration from '../legacy/SleepDuration';
import { ScrollView } from 'react-native-gesture-handler';

const initialWakeupTarget: SleepTimePoint = { hour: 8, minute: 0};
const initialSleepTarget: SleepTimePoint = { hour: 24, minute: 0 };

const initSleepData: Array<SleepDuration> = [
    new SleepDuration({ hour: 23, minute: 5 }, { hour: 7, minute: 45 }, "2020-04-24", 4),
    new SleepDuration({ hour: 23, minute: 0 }, { hour: 7, minute: 30 }, "2020-04-23", 4),
    new SleepDuration({ hour: 23, minute: 0 }, { hour: 9, minute: 36 }, "2020-04-22", 4)
];

/**
 * @brief Home page fo SleepWell, also konwn as Dashboard
 */
export const Home: React.FC = () => {
    /** User's sleep data */
    const [sleepData, setSleepData] = useState(initSleepData);
    const [sleepTarget, setSleepTarget] = useState(initialSleepTarget);
    const [awakeTarget, setAwakeTarget] = useState(initialWakeupTarget);
    /** Date object use to get current date. */
    const today = new Date();
    const lastSlepStat: SleepTimePoint =
        (sleepData.length <= 0 ? { hour: 0, minute: 0 } : sleepData[0].getDuration());

    const onAddData = (data: SleepDuration) => {
        setSleepData(prev => ([data, ...prev]));
    };

    return (
        <View
            style={{
                ...gstyles.container
            }}
        >
            <ScrollView>
                <View style={{ marginVertical: 16-5 }}>
                    <OverviewCard
                        today={ today }
                        lastSleptStat={ lastSlepStat }
                        onAddData={ onAddData }
                    />
                    <WeeklyDeatils
                        sleepData={ sleepData }
                        awakeTarget={ awakeTarget }
                        sleepTarget={ sleepTarget }
                    />
                    <AverageDataCard sleepData={ sleepData }/>
                </View>
            </ScrollView>
        </View>
    );
}

/**
 * Styles used internally for Home
 */
const styles = StyleSheet.create({
    
});