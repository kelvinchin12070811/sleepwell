import { FontAwesome5 } from '@expo/vector-icons';
import { DrawerNavigationProp } from '@react-navigation/drawer';
import React from 'react';
import { View, Dimensions, StyleSheet } from 'react-native';

import { gstyles } from '../styles/Global';
import { scheme } from '../styles/Scheme';
import { ThemedText } from './ThemedText';

/**
 * @brief Type of the props of Header.
 */
interface HeaderProps
{
    navigation: DrawerNavigationProp<any, any>, /**< navigation object for navigating purpose. */
    text: string /**< Title of the Header */
}

/**
 * @brief Header is the component that draw on top of each text, it is used for navigating the SleepWell app.
 * @param props Properties of the Header, currently two component is required @sa HeaderProps
 */
export const Header: React.FC<HeaderProps> = ({ navigation, text }) => {
    /**
     * Event callback of drawer open button.
     */
    const drawerOpener = () => {
        navigation.openDrawer();
    };

    return (
        <View style={ styles.header }>
            <FontAwesome5 name="bars" size={ 18 } style={ styles.icon } onPress={ drawerOpener }/>
            <View>
                <ThemedText style={ styles.headerTitle }>{ text }</ThemedText>
            </View>
        </View>
    );
};

/**
 * getHeader is the function that return the header component to the options prop of the Stack.Screen
 * component.
 * @param props Original props from the Stack.Screen.
 * @param title Title of the header.
 */
export const getHeader: any = (props: any, title: string) => {
    return {
        headerTitle: () => (<Header navigation={ props.navigation } text={ title }/>)
    }
};

/* Styles of the Header */

/**
 * getHeaderOption is the function that return the option use to style the header. This function is
 * passed to the drawerContentOptions prop of the Navigation.Navigator.
 */
export const getHeaderOption: any = () => {
    return {
        activeBackgroundColor: scheme.theme.l3.bgcolor,
        activeTintColor: scheme.theme.l3.color,
        inactiveTintColor: scheme.theme.d1.color,
        labelStyle: { fontSize: 18 }
    };
}

/**
 * getHeaderPlaceholdrSytle is the function that return the style of the placeholder of the header.
 * This function is passed to the drawerStyle prop of the Navigation.Navigator
 */
export const getHeaderPlaceholderStyle: any = () => {
    return { backgroundColor: scheme.theme.l1.bgcolor };
}

/**
 * getHeaderStyle is the function that return the style of the header, this function is pass to
 * screenOptions props of the Stack.Navigator component.
 */
export const getHeaderStyle: any = () => {
    return {
        headerStyle: {
            backgroundColor: scheme.theme.l1.bgcolor,
        },
        headerTitleAlign: "center"
    };
};

/**
 * Inner styles of the Header
 */
const styles = StyleSheet.create({
    /** Main header style */
    header: {
        width: Dimensions.get("screen").width,
        height: "100%",
        alignItems: "center",
        justifyContent: "center",
    },
    /** Title style of header */
    headerTitle: {
        fontSize: 20,
        fontWeight: "bold"
    },
    /** Icon style of header */
    icon: {
        position: "absolute",
        left: 16,
        color: scheme.theme.l1.color
    }
});