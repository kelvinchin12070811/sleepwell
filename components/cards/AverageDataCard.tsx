import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';

import { Card } from './Card';
import { ThemedText } from '../ThemedText';
import { gstyles } from '../../styles/Global';
import SleepDuration from '../../legacy/SleepDuration';

/** Props for AverageDataCard */
interface AverageDataCardProps
{
    sleepData: Array<SleepDuration>; /**< Users sleep data. */
}

/**
 * Display average sleep data of pass 7 days.
 * Props refer to AverageDataProps.
 */
export const AverageDataCard: React.FC<AverageDataCardProps> = ({ sleepData }) => {
    return(
        <Card style={{ ...gstyles.cards, paddingHorizontal: 0 }}>
            <ThemedText style={{ marginBottom: 15 }}>Average Sleep Detail</ThemedText>
            <View style={ gstyles.sleepDetailPanel }>
                <View style={{ ...gstyles.sleepDetailPanelChild }}>
                    <MaterialIcons style={ gstyles.sleepDetailPanelChildIcon } name="timer" size={ 50 }/>
                    <ThemedText>Duration</ThemedText>
                    <ThemedText>
                    {(() => {
                        let avg: number = 0;

                        sleepData.forEach((itr, idx) => {
                            if (idx >= 7) return;
                            let cur = itr.getDuration();
                            avg += cur.hour + (cur.minute / 60);
                        });

                        const days = sleepData.length > 7 ? 7 : sleepData.length;
                        avg /= (days <= 0 ? 1 : days);
                        const hour = Math.floor(avg);
                        const minute = Math.floor((avg - hour) * 60);
                        return `${ hour } h ${ minute } m`;
                    })()}
                    </ThemedText>
                </View>
                <View style={ gstyles.sleepDetailPanelChild }>
                    <MaterialCommunityIcons
                        style={ gstyles.sleepDetailPanelChildIcon }
                        name="weather-sunset"
                        size={ 50 }
                    />
                    <ThemedText>Awake</ThemedText>
                    <ThemedText>
                    {(() => {
                        let avg: number = 0;
                        
                        sleepData.forEach((itr, idx) => {
                            if (idx >= 7) return;
                            let cur = itr.getAwakeTime().hour + (itr.getAwakeTime().minute / 60);
                            avg += cur;
                        });

                        const days = sleepData.length > 7 ? 7 : sleepData.length;
                        avg /= (days <= 0 ? 1 : days);
                        const hour = Math.floor(avg);
                        const minute = Math.floor((avg - hour) * 60);
                        return `${ hour > 12 ? hour - 12 : hour }:` +
                            `${ minute >= 10 ? "" : "0" }` +
                            `${ minute } ` +
                            `${ hour >= 12 ? "PM" : "AM" }`;
                    })()}
                    </ThemedText>
                </View>
                <View style={ gstyles.sleepDetailPanelChild }>
                    <MaterialCommunityIcons
                        style={ gstyles.sleepDetailPanelChildIcon }
                        name="weather-night"
                        size={ 50 }
                    />
                    <ThemedText>Sleep</ThemedText>
                    <ThemedText>
                    {(() => {
                        let avg: number = 0;
                        
                        sleepData.forEach((itr, idx) => {
                            if (idx >= 7) return;
                            const cur = itr.getSleptTime();
                            avg += cur.hour + (cur.minute / 60);
                        });

                        const days = sleepData.length > 7 ? 7 : sleepData.length;
                        avg /= (days <= 0 ? 1 : days);
                        const hour = Math.floor(avg);
                        const minute = Math.floor((avg - hour) * 60);
                        return `${ hour > 12 ? hour - 12 : hour }:` +
                            `${ minute >= 10 ? "" : "0" }` +
                            `${ minute } ` +
                            `${ hour >= 12 ? "PM" : "AM" }`;
                    })()}
                    </ThemedText>
                </View>
            </View>
        </Card>
    );
};