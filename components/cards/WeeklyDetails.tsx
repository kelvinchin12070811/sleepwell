import { Entypo, MaterialCommunityIcons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';

import { Card } from './Card';
import { ThemedText } from '../ThemedText';
import { gstyles } from '../../styles/Global';
import { scheme } from '../../styles/Scheme';
import SleepDuration from '../../legacy/SleepDuration';

interface WeeklyDetailsProps
{
    sleepData: Array<SleepDuration>; /**< Data of last sleep. */
    awakeTarget: SleepTimePoint; /**< User's awake target. */
    sleepTarget: SleepTimePoint; /**< User's sleep target. */
}

/**
 * Card that list out last 7 days detail of user's sleep.
 * Take 3 required props, refer WeeklyDetailsProps.
 */
export const WeeklyDeatils: React.FC<WeeklyDetailsProps> = ({ sleepData, awakeTarget, sleepTarget }) => {
    const navigation = useNavigation();

    const onDetailListBtnPressed = () => {
        navigation.navigate("DetailList", { sleepData: sleepData });
    };

    const getWeakDayName = (date: Date) => {
        switch (date.getDay())
        {
        case 1:
            return "Mon";
        case 2:
            return "Tue";
        case 3:
            return "Wed";
        case 4:
            return "Thu";
        case 5:
            return "Fri";
        case 6:
            return "Sat";
        default:
            return "Sun";
        }
    };

    return (
        <Card style={{ ...gstyles.cards, alignItems: "stretch" }}>
            <ThemedText style={{ textAlign: "center", marginBottom: 12 }}>Sleep Target</ThemedText>
            <View style={{ alignItems: "stretch", paddingHorizontal:5 }}>
            {
                sleepData.map((itr, idx) => {
                    if (idx >= 7) return(null);
                    const cur = new Date(itr.getDate());
                    const sleptTime = itr.getSleptTime();
                    const awakeTime = itr.getAwakeTime();
                    return (
                        <View key={ idx } style={{
                            justifyContent: "space-between",
                            alignItems: "center",
                            flexDirection: "row"
                        }}>
                            <ThemedText>{`${cur.getDate()} ${ getWeakDayName(cur) }`}</ThemedText>
                            <View style={{ flexDirection: "row"}}>
                                <MaterialCommunityIcons
                                    style={ gstyles.sleepDetailPanelChildIcon }
                                    size={ 20 }
                                    name="weather-night"
                                />
                                <ThemedText>{sleptTime.hour}:{sleptTime.minute}</ThemedText>
                            </View>
                            <View>
                            {
                                (sleptTime.hour + sleptTime.minute / 60 < sleepTarget.hour + sleepTarget.minute / 60) ?
                                <MaterialCommunityIcons name="check-circle" size={20} style={{ color: "white"}}/> :
                                null
                            }
                            </View>
                            <Entypo name="arrow-bold-right" size={20} style={{ color: "white" }}/>
                            <View style={{ flexDirection: "row"}}>
                                <MaterialCommunityIcons
                                    style={gstyles.sleepDetailPanelChildIcon}
                                    size={20}
                                    name="weather-sunset"
                                />
                                <ThemedText>{awakeTime.hour}:{awakeTime.minute}</ThemedText>
                            </View>
                            <View>
                            {
                                (awakeTime.hour + awakeTime.minute / 60 < awakeTarget.hour + awakeTarget.minute / 60) ?
                                <MaterialCommunityIcons name="check-circle" size={20} style={{ color: "white"}}/> :
                                null
                            }
                            </View>
                        </View>
                    );
                })
            }
            </View>
            <TouchableOpacity
                style={ styles.button }
                onPress={ onDetailListBtnPressed }
            >
                <MaterialCommunityIcons
                    name="more"
                    size={ 30 }
                    style={ styles.buttonLabel }
                />
                <ThemedText>Show All</ThemedText>
            </TouchableOpacity>
        </Card>
    );
};

/** Styles for AverageDataCard internally */
const styles = StyleSheet.create({
    /** Button style */
    button: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: scheme.theme.l2.bgcolor,
        marginTop: 20,
        paddingVertical: 8,
        borderRadius: 30
    },
    buttonLabel: {
        color: scheme.theme.l2.color,
        marginRight: 8
    }
});